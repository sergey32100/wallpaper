/*******************************
 *
 * Created: 04/17/2017
 *
 *******************************/


(function() {
    "use strict";
    
    var app = angular
        .module("PopWalls", [
            // libs
            "ngRoute", 
            "ui.bootstrap", 
            
            // service model
            "PW.services", 
            
            // controllers for views
            "PW.top", 
            "PW.topHeader", 
            "PW.topFooter", 
            
            "PW.home",
            "PW.internal",
            "PW.productInternal"
        ])
        .config(["$routeProvider", function($routeProvider){

            $routeProvider
                .when("/", {
                    title: "Home",
                    controller: "home",
                    templateUrl: "./app/views/home.tpl"
                })
                .when("/internal/:category", {
                    title: "Internal",
                    controller: "internal",
                    templateUrl: "./app/views/internal.tpl"
                })
                .when("/internal/:category/product-internal/:product_id", {
                    title: "Product Internal",
                    controller: "productInternal",
                    templateUrl: "./app/views/product-internal.tpl"
                })
                .otherwise({
                    redirectTo: "/"
                });

        }])
        .run(["$rootScope", function($rootScope){
            $rootScope.$on("$routeChangeSuccess", function(event, current, previous){
                $rootScope.title = current.$$route.title;
            });
        }]);

})();