/*******************************
 *
 * Created: 04/17/2017
 *
 *******************************/

(function() {
    "use strict";
    
    angular.module("PW.home", []).controller("home", ["$scope", "$api", "$interval", "$timeout", function($scope, $api, $interval, $timeout) {
        // enable popup
        $scope.$parent.popupActive = true;
    }]);
    
})();
