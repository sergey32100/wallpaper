/*******************************
 *
 * Created: 04/20/2017
 *
 *******************************/

(function() {
    "use strict";
    
    angular.module("PW.internal", []).controller("internal", ["$scope", "$api", "$interval", "$timeout", function($scope, $api, $interval, $timeout) {
        
        //
    }])
    .filter("range", function() {
        return function(input, total) {
            total = parseInt(total);

            for (var i = 0; i < total; i++) {
                input.push(i);
            }

            return input;
        };
    });
    
})();
