/*******************************
 *
 * Created: 04/20/2017
 *
 *******************************/


(function() {
    "use strict";
    
    angular.module("PopWalls")
        .directive("bannerSlider", ["$interval", function($interval){
            return {
                restrict : "A",
                scope: {
                    bannerDuration: "=bannerSlider"
                },
                link: function($scope, elem, attrs){
                    window.bannerSlider = 0;
                    var autoBannerSlider = function(elem){                        
                        elem.find(".active").removeClass("active");
                        
                        window.bannerSlider++;
                        window.bannerSlider = window.bannerSlider >= elem.children().length? 0: window.bannerSlider;
                        elem.children().eq(window.bannerSlider).addClass("active");
                    };
                    
                    $interval(function() {
                        autoBannerSlider(elem);
                    }, attrs.bannerSlider);
                }
            };
        }]);

})();