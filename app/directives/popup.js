/*******************************
 *
 * Created: 04/21/2017
 *
 *******************************/


(function() {
    "use strict";
    
    angular.module("PopWalls")
        .directive("ngPopup", ["$timeout", function($timeout){
            return {
                restrict: "E",
                link: function($scope, elem, attrs){
                    $timeout(function(){
                        if($scope.$parent.popupActive){
                            elem.addClass("active").children(".popup-bg").click(function(e){
                                elem.removeClass("active");
                            });
                            elem.find("#popup-close").click(function(e){
                                elem.removeClass("active");
                            });
                        }                    
                    }, 300 + parseInt(attrs.popupDelay));
                }  
            };
        }]);

})();