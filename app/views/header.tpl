<!---------------------------------------------
-- Header template
----------------------------------------------->

<div id="topHeader">
    <div class="section-first clearfix">
        <div class="row">
            <div class="col-md-6 text-left">
                <a href="#" id="sel-country">
                    <img src="./assets/imgs/icons/MISC/flag-UK.png"/>
                    <span class="color-white bold">Free Delivery to The UK</span>
                </a>
                <a href="tel:02380664818" id="callus">
                    <span>Call us on:</span> 02380 664818
                </a>
            </div>
            <div class="col-md-6 text-right">
                <div class="short-menu">
                    <a href="#" class="bold">My Account</a>
                    <a href="#" id="sel-cart" class="bold">Basket <img src="./assets/imgs/icons/MISC/icon-cart.png" class="cart-icon"> <span class="color-white cart-num">1</span></a>
                    <a href="#" class="bold">Help</a>
                </div>
                <div class="social-icons">
                    <a href="#"><img src="./assets/imgs/icons/Social media Icons/pinterest.svg"></a>
                    <a href="#"><img src="./assets/imgs/icons/Social media Icons/facebook.svg"></a>
                    <a href="#"><img src="./assets/imgs/icons/Social media Icons/google-plus.svg"></a>
                    <a href="#"><img src="./assets/imgs/icons/Social media Icons/twitter.svg"></a>
                </div>
            </div>
        </div>
    </div>
    <div class="section-second">
        <div class="links text-center">
            <a href="#"><span><img src="./assets/imgs/icons/Pop Walls Icons/Icon-Peel.png"> Repositionable</span></a>
            <a href="#"><span><img src="./assets/imgs/icons/Pop Walls Icons/Icon-NoPaste.png"> No Paste</span></a>
            <a href="#"><span><img src="./assets/imgs/icons/Pop Walls Icons/Icon-Wipe.png"> Wipeable</span></a>
            <a href="#"><span><img src="./assets/imgs/icons/Pop Walls Icons/Icon-Light.png"> Exclusive Designs</span></a>
        </div>
        <div class="nav-wrap text-center clearfix">
            <a href="#" id="logo"><img src="./assets/imgs/Logo/Pop Walls Dark.png"></a>
            
            <ul class="nav">
                <li><a href="#/" ng-class="$parent.title == 'Home'? 'active': ''">HOME</a></li>
                <li>
                    <a href="#/internal/all" ng-class="$parent.title == 'Internal'? 'active': ''">SHOP</a>
                    <div class="subnav">
                        <div class="subnav-wrap subnav-4">
                            <div class="subnav-col-4">
                                <label class="color-purple">NEW IN STOCK!</label>
                                <ul>
                                    <li><a href="#/internal/bedroom">Bedroom</a></li>
                                    <li><a href="#/internal/livingroom">Living Room</a></li>
                                    <li><a href="#/internal/diningroom">Dining Room</a></li>
                                </ul>
                            </div>
                            <div class="subnav-col-4">
                                <label>COLOUR</label>
                                <ul>
                                    <li><a href="#/internal/pinkspurples">Pinks & Purples</a></li>
                                    <li><a href="#/internal/greens">Greens</a></li>
                                    <li><a href="#/internal/multicolured">Multi-coloured</a></li>
                                </ul>
                            </div>
                            <div class="subnav-col-4">
                                <label>COLLECTION</label>
                                <ul>
                                    <li><a href="#/internal/bigjapan">Big in Japan</a></li>
                                    <li><a href="#/internal/famousfreehand">Famous Freehand</a></li>
                                    <li><a href="#/internal/foldedadventure">Folded Adventure</a></li>
                                </ul>
                            </div>
                            <div class="subnav-col-4">
                                <label>STYLE</label>
                                <ul>
                                    <li><a href="#/internal/abstract">Abstract</a></li>
                                    <li><a href="#/internal/colourful">Colourful</a></li>
                                    <li><a href="#/internal/contemporary">Contemporary</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </li>
                <li><a href="#/">ABOUT US</a></li>
                <li><a href="#/">FAQ's</a></li>
                <li><a href="#/">BLOG</a></li>
                <li><a href="#/">FOR BUSINESS</a></li>
            </ul>
            <div class="pull-right d-block search-wrap">
                <input type="text" id="search" placeholder="Search">
            </div>
        </div>
    </div>
    
    <div class="container" ng-if="$parent.title != 'Home'">
        <div class="breadcrumb">
            <a href="#/" class="regular">Home</a>
            <span class="regular">></span>
            <a href="#/internal/all" class="regular">Shop</a>
            <span class="regular">></span>
            <span class="regular">Categories</span>
            <span class="regular">:</span>
            <a href="#/internal/all" class="regular">All</a>            
            <span class="regular">></span>
            <span class="regular">WallPaper Name</span>
        </div>
    </div>
</div>




