<!---------------------------------------------
-- Product Internal template
----------------------------------------------->

<div id="productInternal" class="container">
    
    <div class="row wallpaper-wrap">
        <div class="col-md-8">
            <div class="wallpaper-full"><img src="./assets/imgs/Sample-Contents/wallpaper-706x470.jpg"></div>
            <div class="clearfix wallpaper-thumbs">
                <img src="./assets/imgs/Sample-Contents/wallpaper-224x150.jpg">
                <img src="./assets/imgs/Sample-Contents/wallpaper-224x150.jpg">
                <img src="./assets/imgs/Sample-Contents/wallpaper-224x150.jpg">
            </div>
        </div>
        
        <div class="col-md-4">
            <div class="color-darkgrey wallpaper-name">Wallpaper Name</div>
            <div class="color-grey artist-name">Artist Name</div>
            <div class="star-group">
                <span class="active"></span>
                <span class="active"></span>
                <span class="active"></span>
                <span class="active"></span>
                <span></span>
            </div>
            
            <div class="getstarted text-center full-color-white">
                <label>Get Started</label>
                <p>Enter the size of your wall, and remember to add 5cm’s to each side!</p>
                <div class="clearfix size-group">
                    <input type="number" id="wallpaper-height" placeholder="HEIGHT(CM)" ng-model="addtobasketHeight"/>
                    <input type="number" id="wallpaper-width" placeholder="WIDTH(CM)" ng-model="addtobasketWidth"/>
                </div>
                <div class="action-group">
                    <input type="button" id="addtobasket" value="Add To Basket"/>
                    <div><i>- or-</i></div>
                    <a href="#" id="orderasample" class="color-white">Order a Sample</a>
                </div>
            </div>
            
            <div class="include-info">
                <h3>INCLUDED IN YOUR ORDER</h3>
                
                <ul>
                    <li>Hanging Instructions</li>
                    <li>Free Delivery</li>
                    <li>1 years Warrenty</li>
                    <li>14 Day Return Policy</li>
                </ul>
            </div>
        </div>
    </div>
    
    <div class="row other-info">
        <div class="col-md-12">
            <div class="tabs-wrap">
                <ul class="tabs">
                    <li ng-click="selTab = 'artist'" ng-class="!selTab || selTab == 'artist'? 'active': ''">ARTIST PROFILE</li>
                    <li ng-click="selTab = 'product'" ng-class="selTab == 'product'? 'active': ''">PRODUCT SPECIFICATION</li>
                    <li ng-click="selTab = 'returns'" ng-class="selTab == 'returns'? 'active': ''">RETURNS POLICY</li>
                    <li ng-click="selTab = 'customer'" ng-class="selTab == 'customer'? 'active': ''">CUSTOMER REVIEWS</li>
                </ul>
                <div class="tabs-items">
                    <div class="tabs-item artist-profile" ng-class="!selTab || selTab == 'artist'? 'active': ''">
                        <div class="artist-avatar"><img src="./assets/imgs/Artists heads/Rob-lowe.png"/></div>
                        <div class="artist-info">
                            <span class="artist-name">MEET ROB LOWE</span>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
                            <div class="text-right readmore"><a href="#">Read More ></a></div>
                        </div>
                    </div>
                    <div class="tabs-item" ng-class="selTab == 'product'? 'active': ''">
                        test
                    </div>                
                </div>
            </div>
        </div>
    </div>
    
    <div class="row review-wrap">
        <div class="col-md-12">
            <div class="clearfix review-form-title">
                <h2 class="form-title">Leave a Review</h2>

                <div class="star-group editable">
                    <span ng-class="leaveReview > 0? 'active': ''" ng-click="leaveReview = 1"></span>
                    <span ng-class="leaveReview > 1? 'active': ''" ng-click="leaveReview = 2"></span>
                    <span ng-class="leaveReview > 2? 'active': ''" ng-click="leaveReview = 3"></span>
                    <span ng-class="leaveReview > 3? 'active': ''" ng-click="leaveReview = 4"></span>
                    <span ng-class="leaveReview > 4? 'active': ''" ng-click="leaveReview = 5"></span>
                </div>
            </div>
            <div class="review-form">
                <textarea id="review-text" ng-model="reviewText"></textarea>
                <input type="button" id="submit-review" value="SUBMIT REVIEW"/>
            </div>
        </div>
    </div>
    
    <div class="row other-design">
        <div class="col-md-12">
            <h2 class="form-title">OTHER DESIGNS FROM THIS ARTIST</h2>

            <div class="row">
                <div class="col-md-4">
                    <div class="wallpaper-image"><img src="./assets/imgs/Sample-Contents/wallpaper-349x233.jpg"/></div>
                    <div class="wallpaper-info">
                        <div class="wallpaper-name-price">
                            <span class="wallpaper-name">WALL PAPER NAME</span>
                            <span class="color-purple wallpaper-price">£45 p/m</span>
                        </div>
                        <div class="color-grey wallpaper-name-artist">
                            <span class="color-grey">Artist Name</span>
                        </div>
                        <div class="star-group">
                            <span class="active"></span>
                            <span class="active"></span>
                            <span class="active"></span>
                            <span class="active"></span>
                            <span class="active"></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="wallpaper-image"><img src="./assets/imgs/Sample-Contents/wallpaper-349x233.jpg"/></div>
                    <div class="wallpaper-info">
                        <div class="wallpaper-name-price">
                            <span class="wallpaper-name">WALL PAPER NAME</span>
                            <span class="color-purple wallpaper-price">£45 p/m</span>
                        </div>
                        <div class="color-grey wallpaper-name-artist">
                            <span class="color-grey">Artist Name</span>
                        </div>
                        <div class="star-group">
                            <span class="active"></span>
                            <span class="active"></span>
                            <span class="active"></span>
                            <span class="active"></span>
                            <span class="active"></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="wallpaper-image"><img src="./assets/imgs/Sample-Contents/wallpaper-349x233.jpg"/></div>
                    <div class="wallpaper-info">
                        <div class="wallpaper-name-price">
                            <span class="wallpaper-name">WALL PAPER NAME</span>
                            <span class="color-purple wallpaper-price">£45 p/m</span>
                        </div>
                        <div class="wallpaper-name-artist">
                            <span class="color-grey">Artist Name</span>
                        </div>
                        <div class="star-group">
                            <span class="active"></span>
                            <span class="active"></span>
                            <span class="active"></span>
                            <span class="active"></span>
                            <span class="active"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>




