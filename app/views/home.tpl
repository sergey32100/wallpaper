<!---------------------------------------------
-- Main Page template
----------------------------------------------->

<div id="home">
    <div class="banner-wrap">
        <div class="banner-items" banner-slider="5000">
            <img src="./assets/imgs/Sample-Contents/banner-1920x820-1.jpg" class="active"/>
            <img src="./assets/imgs/Sample-Contents/banner-1920x820-2.jpg"/>
            <img src="./assets/imgs/Sample-Contents/banner-1920x820-3.jpg"/>
        </div>
        <a href="#/" class="color-purple banner-btn">Shop Now</a>
    </div>
    
    <div class="steps-wrap">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center form-title">
                    <h2>MADE-TO-MEASURE, SELF ADHESIVE, BEAUTIFUL WALLPAPER</h2>
                    <span>IN THREE EASY STEPS</span>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <h2 class="form-sub-title">WHICH?</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco qui ofveritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                </div>
                <div class="col-md-4">
                    <h2 class="form-sub-title">DESIGN?</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco qui ofveritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                </div>
                <div class="col-md-4">
                    <h2 class="form-sub-title">CLICK?</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco qui ofveritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                </div>
            </div>
        </div>
    </div>
    
    <div class="decorating-wrap">
        <div class="text-center form-title">
            <h2>WHICH ROOM ARE YOU DECORATING?</h2>
        </div>

        <div class="rooms-wrap clearfix">
            <div class="rooms-item">
                <img src="./assets/imgs/Sample-Contents/room-330x510-1.jpg"/>
                <div class="btn-wrap">
                    <a href="#/" class="btn-gotoroom">BEDROOM</a>
                    <a href="#/" class="link-shopnow">SHOP NOW ></a>
                </div>
            </div>
            <div class="rooms-item">
                <img src="./assets/imgs/Sample-Contents/room-330x510-2.jpg"/>
                <div class="btn-wrap">
                    <a href="#/" class="btn-gotoroom">LIVING ROOM</a>
                    <a href="#/" class="link-shopnow">SHOP NOW ></a>
                </div>
            </div>
            <div class="rooms-item">
                <img src="./assets/imgs/Sample-Contents/room-330x510-3.jpg"/>
                <div class="btn-wrap">
                    <a href="#/" class="btn-gotoroom">CHILDRENS</a>
                    <a href="#/" class="link-shopnow">SHOP NOW ></a>
                </div>
            </div>
            <div class="rooms-item">
                <img src="./assets/imgs/Sample-Contents/room-330x510-4.jpg"/>
                <div class="btn-wrap">
                    <a href="#/" class="btn-gotoroom">BATHROOM</a>
                    <a href="#/" class="link-shopnow">SHOP NOW ></a>
                </div>
            </div>
            <div class="rooms-item">
                <img src="./assets/imgs/Sample-Contents/room-330x510-5.jpg"/>
                <div class="btn-wrap">
                    <a href="#/" class="btn-gotoroom">OFFICE/WORKSPACE</a>
                    <a href="#/" class="link-shopnow">SHOP NOW ></a>
                </div>
            </div>
        </div>
        
        <div class="text-center show-everything">
            <a href="#/" class="btn-everything">SHOW ME EVERTHING</a>
        </div>
        
        <div class="container mypopwall-wrap">
            <div class="text-center form-title">
                <h2>#MYPOPWALL</h2>
                <span>Share with us your wonderful space, these people have!</span>
            </div>
            <div class="text-center">
                <div class="mypopwall-items">
                    <img src="./assets/imgs/Sample-Contents/popwall-200x200-1.jpg"/>
                    <img src="./assets/imgs/Sample-Contents/popwall-200x200-2.jpg"/>
                    <img src="./assets/imgs/Sample-Contents/popwall-200x200-3.jpg"/>
                    <img src="./assets/imgs/Sample-Contents/popwall-200x200-4.jpg"/>
                    <img src="./assets/imgs/Sample-Contents/popwall-200x200-5.jpg"/>
                </div>
            </div>
        </div>
    </div>
</div>