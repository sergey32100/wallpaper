<!---------------------------------------------
-- Internal Page template
----------------------------------------------->

<div id="internal" class="clearfix">
    <div class="left-widget">
        <div class="widget-wrap">
            <div class="sub-widget">
                <div class="sub-widget-title">CATEGORIES</div>
                <div class="sub-widget-cont">
                    <ul class="categories">
                        <li><a href="#/internal/category-1">CATEGORY ONE</a></li>
                        <li><a href="#/internal/category-2">CATEGORY TWO</a></li>
                        <li><a href="#/internal/category-3">CATEGORY THREE</a></li>
                        <li><a href="#/internal/category-4">CATEGORY FOUR</a></li>
                        <li><a href="#/internal/category-5">CATEGORY FIVE</a></li>
                    </ul>
                </div>
            </div>
            <div class="sub-widget">
                <div class="sub-widget-title">FILTER BY</div>
                <div class="sub-widget-cont">
                    <ul class="filterby">
                        <li>
                            <span class="collapsible-btn {{collapseColour? 'collapsed': ''}}" ng-click="collapseColour = collapseColour == 1? 0: 1"></span>
                            <a href="#/internal/colour-all">COLOUR</a>
                            <ul ng-class="collapseColour? 'collapsed': ''">
                                <li><a href="#/internal/colour-red">Red</a></li>
                                <li><a href="#/internal/colour-yellow">Yellow</a></li>
                                <li><a href="#/internal/colour-blue">Blue</a></li>
                                <li><a href="#/internal/colour-green">Green</a></li>
                            </ul>
                        </li>
                        <li>
                            <span class="collapsible-btn {{collapseArtist? 'collapsed': ''}}" ng-click="collapseArtist = collapseArtist == 1? 0: 1"></span>
                            <a href="#/internal/artist-all">ARTIST</a>
                            <ul ng-class="collapseArtist? 'collapsed': ''">
                                <li><a href="#/internal/artist-1">Artist 1</a></li>
                                <li><a href="#/internal/artist-2">Artist 2</a></li>
                                <li><a href="#/internal/artist-3">Artist 3</a></li>
                                <li><a href="#/internal/artist-4">Artist 4</a></li>
                            </ul>
                        </li>
                        <li>
                            <span class="collapsible-btn {{collapseRoom? 'collapsed': ''}}" ng-click="collapseRoom = collapseRoom == 1? 0: 1"></span>
                            <a href="#/internal/room-all">ROOM</a>
                            <ul ng-class="collapseRoom? 'collapsed': ''">
                                <li><a href="#/internal/room-bedroom">BEDROOM</a></li>
                                <li><a href="#/internal/room-livingroom">LIVING ROOM</a></li>
                                <li><a href="#/internal/room-childrens">CHILDRENS</a></li>
                                <li><a href="#/internal/room-bathroom">BATHROOM</a></li>
                                <li><a href="#/internal/room-office">OFFICE/WORKSPACE</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    
    <div class="products-section">
        <div class="featured">
            <div class="featured-wrap">
                <h2>Featured Artist</h2>
                <img src="./assets/imgs/Sample-Contents/featured-artist-184x123.jpg"/>
                <p class="featured-desc">
                    Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </p>
                <a href="#/" class="featured-readmore">READ MORE ></a>
            </div>
        </div>
        
        <div class="filter-section">
            <span class="searched-total">{{15129 | number}} products</span>
            <label>
                Show 
                <select id="numberperpage">
                    <option value="9">9 per page</option>
                    <option value="12">12 per page</option>
                    <option value="24">24 per page</option>
                    <option value="48">48 per page</option>
                </select>
            </label>
            <label>
                Sort By  
                <select id="sortby">
                    <option value="latest">Latest additions first</option>
                </select>
            </label>
        </div>
        
        <div class="searched-products clearfix">
            <div class="product-item" ng-repeat="n in [] | range:9">
                <div class="product-img">
                    <img src="./assets/imgs/Sample-Contents/wallpaper-450x300.jpg"/>
                    <a href="#/internal/all/product-internal/1" class="detail">VIEW</a>
                    <div class="sale">SALE</div>
                </div>                
                <div class="product-info">
                    <div class="product-name-price">
                        <span class="product-name">WALL PAPER NAME</span>
                        <span class="color-purple bold product-price">£45 p/m</span>
                    </div>
                    <div class="product-name-artist">
                        <span class="color-grey">Artist Name</span>
                    </div>
                    <div class="star-group">
                        <span class="active"></span>
                        <span class="active"></span>
                        <span class="active"></span>
                        <span class="active"></span>
                        <span class="active"></span>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="searched-pagination">
            <a href="" class="next-page">&lt;</a>
            <span class="current-page">PAGE 01 OF 157</span>
            <a href="" class="prev-page">&gt;</a>
        </div>
    </div>
</div>