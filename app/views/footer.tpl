<!---------------------------------------------
-- Footer template
----------------------------------------------->

<div id="topFooter">
    <div class="testimonial-wrap">
        <div class="row text-center">
            <span class="asseenin">As Seen In:</span>
            <img src="./assets/imgs/icons/Testimonial/Ideal-Home.png">
            <img src="./assets/imgs/icons/Testimonial/Country-Homes.png">
            <img src="./assets/imgs/icons/Testimonial/Wallpaper.png">
            <img src="./assets/imgs/icons/Testimonial/Theguardian.png">
            <img src="./assets/imgs/icons/Testimonial/Good-Househeeping.png">
        </div>
    </div>
    <div class="footer-content">
        <div class="row text-center">
            <div class="col-md-3">
                <div class="contact text-left full-color-grey">
                    <img id="footer-logo" src="./assets/imgs/Logo/Pop Walls White.png"/>
                    <ul>
                        <li class="bold">Pop Walls ltd</li>
                        <li>Brunel Road</li>
                        <li>Totton, Hampshire</li>
                        <li>SO40 3WX</li>
                        <li>Company Number: 798953684</li>
                    </ul>
                </div>
            </div>
            <div class="col-md-6">
                <div class="newsletter text-left">
                    <h2 class="bold">SIGN UP OUR NEWSLETTER</h2>
                    <span>No junk. Just exclusive offer and latest trends</span>
                    
                    <div class="newsletter-form">
                        <input type="email" id="newsletter-email" placeholder="Enter email address"><a href="#" id="subscribe" class="bold">Subscribe</a>
                    </div>
                </div>
                            
                <div class="social-icons text-left">
                    <a href="#"><img src="./assets/imgs/icons/Social media Icons/pinterest.svg"></a>
                    <a href="#"><img src="./assets/imgs/icons/Social media Icons/facebook.svg"></a>
                    <a href="#"><img src="./assets/imgs/icons/Social media Icons/google-plus.svg"></a>
                    <a href="#"><img src="./assets/imgs/icons/Social media Icons/twitter.svg"></a>
                </div>
            </div>
            <div class="col-md-3">
                <div class="customer-care text-left">
                    <ul>
                        <li class="bold">Customer Care</li>
                        <li>- Privacy policy</li>
                        <li>- Returns Policy</li>
                        <li>- Shipping Information</li>
                        <li>- FAQ’s</li>
                        <li>- Terms & Conditions</li>
                        <li>- Privacy Policy</li>
                        <li>- Accessibility</li>
                    </ul>
                </div>
                <div class="payments">
                    <a href="#"><img src="./assets/imgs/icons/Payment Icons/Visa.png"></a>
                    <a href="#"><img src="./assets/imgs/icons/Payment Icons/Mastercard.png"></a>
                    <a href="#"><img src="./assets/imgs/icons/Payment Icons/Apple Pay.png"></a>
                    <a href="#"><img src="./assets/imgs/icons/Payment Icons/PayPal.png"></a>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright text-right full-color-white">
        <span>Copyright 2017  Pop Walls a registered trademark of Hobbs the Printers Ltd. | All rights reserved</span>
    </div>
</div>

<ng-popup popup-delay="1000">
    <div class="popup-wrap">
        <div class="popup-cont">
            <div class="popup-title">
                <h2>10% off your first order</h2>
                <span>WHEN YOU JOIN OUR NEWSLETTER</span>
            </div>
            <div class="popup-desc">
                <ul>
                    <li>Latest inspiration for your home</li>
                    <li>Best deals and offers on Pop Walls products</li>
                    <li>We will never spam you or sell on your details</li>
                </ul>
            </div>
            <div class="popup-form">
                <input type="email" id="popup-email" placeholder="Your email"/>
                <a href="" id="popup-subscribe">SUBSCRIBE</a>
            </div>
        </div>
        <a href="" id="popup-close">CLOSE</a>
    </div>
    <div class="popup-bg"></div>
</ng-popup>


